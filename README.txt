CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Action links
 * Compatibility
 * Support
 * Authors
 * Sponsors


INTRODUCTION
------------

  Fube is a Cube/Rubik sub-theme with some nice enhancements, mostly stolen directly from the Fubik theme.

  * Better integration with core Overlay module
  * Better integration with Views UI
  * Extra icons for administrative pages
  * Nice icons for creating administrative views
  * Small tweaks here and there to improve appearance in many places


REQUIREMENTS
------------

  * Cube Theme (http://drupal.org/project/cube)


INSTALLATION
------------

  1. Install Rubik, following the instructions from: http://drupal.org/project/rubik

  2. Copy the Fube theme to your sites/all/themes folder

  3. Go to Appearance (admin/appearance) and select Fube from the drop-down list under Administrative Theme


ACTION LINKS
------------

  Fube has some nice default classes for adding icons to links and creating beautiful administration views.

  * .action-view (adds a "view" icon)
  * .action-edit (adds an "edit" icon)
  * .action-delete (adds a "delete" icon)
  * .action-top (adds a "go to top" icon)


COMPATIBILITY
-------------

  A lot of work has gone into ensuring maximum compatibility with other contrib modules. If you find a bug please use the issue tracker for support. Thanks!


SUPPORT
-------

  If you find a bug or have a feature request please file an issue: http://drupal.org/node/add/project-issue/cube_simple


AUTHORS
--------

  * Tom Kirkpatrick (mrfelton) - http://drupal.org/user/305669/ & http://www.systemseed.com
