<?php

/**
 * Implements hook_theme_registry_alter().
 */
function fube_theme_registry_alter(&$theme_registry) {
  // set the node forms back to a simple 1 column layout.
  $theme_registry['node_form']['template'] = 'form-simple';
  $theme_registry['taxonomy_form_term']['template'] = 'form-simple';
}

/**
 * Implements hook_css_alter().
 *
 * Add Fubic styles between Rubic and Cube.
 */
function fube_css_alter(&$css) {
  $fubik_css_path = drupal_get_path('theme', 'fubik') . '/css/fubik.css';
  $cube_css_path = drupal_get_path('theme', 'cube') . '/style.css';
  $styles = array();

  // Remove the Fubic style, so we can readd in the correct location.  
  if (isset($css[$fubik_css_path])) {
    $fubic_style = $css[$fubik_css_path];
    unset($css[$fubik_css_path]);
  }
  
  if (!empty($fubic_style)) {
    // Initialize a counter that we can use to track weight.
    $previous_weight = 1;
    foreach ($css as $path => $spec) {
      if ($path == $cube_css_path) {
        $styles[$fubik_css_path] = $fubic_style;
        $styles[$fubik_css_path]['weight'] = ($previous_weight / 2) - 0.00001;
      }
      $previous_weight = $spec['weight'];
      $styles[$path] = $spec;
    }
    $css = $styles;
  }
}

/* ------------------ FROM FUBIK THEME -------------------------------------*/

/**
 * Preprocessor for theme('page').
 */
function fube_preprocess_html(&$vars) {

  $vars['classes_array'][] = 'fube';

  if (_fube_has_tabs()) {
    $vars['classes_array'][] = 'with-local-tasks';
  }
}

/**
 * Preprocessor for theme('page').
 */
function fube_preprocess_page(&$vars) {
  // Process local tasks. Only do this processing if the current theme is
  // indeed Rubik. Subthemes must reimplement this call.
  global $theme;

  if ($theme === 'fube') {
    _rubik_local_tasks($vars);
  }

  // Add body class when we have local tasks.
  // Since we don't have access to body classes here
  //  set a flag for preprocess_html().
  if (!empty($vars['primary_local_tasks'])) {
    _fube_has_tabs(TRUE);
  }

  // Set a page icon for dashboard links.
  if (empty($vars['page_icon_class'])) {
    $vars['page_icon_class'] = ($item = menu_get_item()) ? implode(' ' , _fube_icon_classes($item['href'])) : '';
  }
  
  $path = drupal_get_path('theme', 'fubik') . '/css/fubik.css';
  drupal_add_css(drupal_get_path('theme', 'fubik') . '/css/fubik.css', array('group' => CSS_THEME, 'every_page' => TRUE));
}

/**
 * Override of theme('menu_local_task').
 */
function fube_menu_local_task($variables) {
  $link = $variables['element']['#link'];
  $link_text = $link['title'];

  if (!empty($variables['element']['#active'])) {
    // Add text to indicate active tab for non-visual users.
    $active = '<span class="element-invisible">' . t('(active tab)') . '</span>';

    // If the link does not contain HTML already, check_plain() it now.
    // After we set 'html'=TRUE the link will not be sanitized by l().
    if (empty($link['localized_options']['html'])) {
      $link['title'] = check_plain($link['title']);
    }
    $link['localized_options']['html'] = TRUE;
    $link_text = t('!local-task-title!active', array('!local-task-title' => $link['title'], '!active' => $active));
  }

  // Render child tasks if available.
  $children = '';
  if (element_children($variables['element'])) {
    $children = drupal_render_children($variables['element']);
    $children = "<ul class='secondary-tabs links clearfix'>{$children}</ul>";
  }

  return '<li' . (!empty($variables['element']['#active']) ? ' class="active"' : '') . '>' . l($link_text, $link['href'], $link['localized_options']) . $children . "</li>\n";
}

/**
 * Preprocessor for theme('help').
 */
function fube_preprocess_help(&$vars) {
  $vars['hook'] = 'help';
  $vars['attr']['id'] = 'help-text';
  $class = 'path-admin-help clear-block toggleable';
  $vars['attr']['class'] = isset($vars['attr']['class']) ? "{$vars['attr']['class']} $class" : $class;
  $help = menu_get_active_help();
  if (($test = strip_tags($help)) && !empty($help)) {
    // Thankfully this is static cached.
    $vars['attr']['class'] .= menu_secondary_local_tasks() ? ' with-tabs' : '';

    $vars['is_prose'] = TRUE;
    $vars['layout'] = TRUE;
    $vars['content'] = "<span class='icon'></span>" . $help;

    // Link to help section.
    $item = menu_get_item('admin/help');
    if ($item && $item['path'] === 'admin/help' && $item['access']) {
      $vars['links'] = l(t('More help topics'), 'admin/help');
    }
  }
}

/**
 * Helper function used to pass a value from preprocess_page() to preprocess_html().
 */
function _fube_has_tabs($val = NULL) {
  $vars = &drupal_static(__FUNCTION__, array());

  // If a new value has been passed
  if ($val) {
    $vars = $val;
  }

  return isset($vars) ? $vars : FALSE;
}

/**
 * Generate an icon class from a path.
 * Modified version of _rubik_icon_classes() that allows icon classes for
 * paths with "dashboard" prefixes.
 */
function _fube_icon_classes($path) {
  $classes = array();
  $args = explode('/', $path);
  if ($args[0] === 'dashboard') {
    // Add a class specifically for the current path that allows non-cascading
    // style targeting.
    $classes[] = 'path-'. str_replace('/', '-', implode('/', $args)) . '-';
    while (count($args)) {
      $classes[] = drupal_html_class('path-'. str_replace('/', '-', implode('/', $args)));
      array_pop($args);
    }
    return $classes;
  }
  return array();
}
/**
 * Preprocess function.
 * Adds classes for icons in Taxonomy vocabulary overview page.
 *
 * @see theme_taxonomy_overview_vocabularies()
 */
function fube_preprocess_taxonomy_overview_vocabularies(&$variables) {
  $form =& $variables['form'];

  foreach (element_children($form) as $key) {
    if (isset($form[$key]['name'])) {
      // Add "edit" class.
      if (!isset($form[$key]['edit']['#attributes']['class'])) {
        $form[$key]['edit']['#attributes']['class'] = array();
      }
      $form[$key]['edit']['#attributes']['class'][] = 'action-edit';

      // Add "view" class.
      if (!isset($form[$key]['list']['#attributes']['class'])) {
        $form[$key]['list']['#attributes']['class'] = array();
      }
      $form[$key]['list']['#attributes']['class'][] = 'action-view';

      // Add "add" class.
      if (!isset($form[$key]['add']['#attributes']['class'])) {
        $form[$key]['add']['#attributes']['class'] = array();
      }
      $form[$key]['add']['#attributes']['class'][] = 'action-add';
    }
  }
}

/**
 * Preprocess function.
 * Adds classes for icons in Taxonomy term overview page.
 *
 * @see theme_taxonomy_overview_terms()
 */
function fube_preprocess_taxonomy_overview_terms(&$variables) {
  $form =& $variables['form'];

  foreach (element_children($form) as $key) {
    if (isset($form[$key]['#term'])) {
      // Add "edit" class.
      if (!isset($form[$key]['edit']['#attributes']['class'])) {
        $form[$key]['edit']['#attributes']['class'] = array();
      }
      $form[$key]['edit']['#attributes']['class'][] = 'action-edit';
    }
  }
}
